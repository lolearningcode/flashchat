//
//  WelcomeViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //creating titleLabel animation
        
        titleLabel.text = ""
        //creates an index by which each letter's time interval is multiplied by
        var charIndex = 0.0
        //name of the title
        let titleText = "⚡️FlashChat"
        //for loop iterating through each letter in the titleText
        for letter in titleText {
            print("\(charIndex * 0.1), \(letter)")
            Timer.scheduledTimer(withTimeInterval: 0.1 * charIndex, repeats: false) { (timer) in
                self.titleLabel.text?.append(letter)
            }
            
            charIndex += 1
        }
    }
    
}
